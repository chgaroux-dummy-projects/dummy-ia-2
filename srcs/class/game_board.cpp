#include "../../includes/class/game_board.hpp"

game_board::game_board(unsigned height, unsigned width, unsigned winning_length)
	: _height(height),
	  _width(width),
	  _board_size(height * width),
	  _winning_length(winning_length),
	  _stone_amounts(0),
	  _current_player(1),
	  _last_player(2)
{
	_board = std::vector<std::vector<unsigned> >(_height, std::vector<unsigned>(_width, 0));
	int mid = static_cast<int>(width) / 2;
	if (width % 2)
	{
		searched_col.push_back(mid);
		for (int i = 1; mid - i >= 0; ++i)
		{
			searched_col.push_back(mid + i);
			searched_col.push_back(mid - i);
		}
	}
	else
	{
		for (int i = 0; mid - i - 1 >= 0; ++i)
		{
			searched_col.push_back(mid - i - 1);
			searched_col.push_back(mid + i);
		}
	}
}

game_board::game_board(game_board const &copy)
	: _height(copy._height),
	  _width(copy._width),
	  _board_size(copy._board_size),
	  _winning_length(copy._winning_length),
	  _stone_amounts(copy._stone_amounts),
	  _current_player(copy._current_player),
	  _last_player(copy._last_player),
	  searched_col(copy.searched_col)
{
	if (this == &copy)
		return;
	_board = copy._board;
}

int game_board::get_board_size() const
{
	return _board_size;
}

int game_board::get_stone_amounts() const
{
	return _stone_amounts;
}

int game_board::get_width() const
{
	return _width;
}

std::vector<std::vector<unsigned> > const &game_board::get_board() const
{
	return _board;
}

int game_board::empty_row(unsigned col) const
{
	for (int i = _height - 1; i >= 0; --i)
		if (_board[i][col] == 0)
			return i;
	return -1;
}

int game_board::occupied_row(unsigned col) const
{
	int empty = empty_row(col);

	if (empty == static_cast<int>(_height - 1))
		return -1;
	return empty + 1;
}

bool game_board::is_winning_move(unsigned col) const
{
	int row;

	if ((row = empty_row(col)) < 0)
		return false;

	// checking if it ends

	// horizontal
	unsigned count = 1;
	for (int y = col - 1; y >= 0 && _board[row][y] == _current_player; --y)
		++count;
	for (int y = col + 1; y < static_cast<int>(_width) && _board[row][y] == _current_player; ++y)
		++count;
	if (count >= _winning_length)
		return true;

	// vertical
	count = 1;
	for (int x = row - 1; x >= 0 && _board[x][col] == _current_player; --x)
		++count;
	for (int x = row + 1; x < static_cast<int>(_height) && _board[x][col] == _current_player; ++x)
		++count;
	if (count >= _winning_length)
		return true;

	// diagonal
	count = 1;
	for (int x = row - 1, y = col - 1; x >= 0 && y >= 0 && _board[x][y] == _current_player; --x, --y)
		++count;
	for (int x = row + 1, y = col + 1; x < static_cast<int>(_height) && y < static_cast<int>(_width) && _board[x][y] == _current_player; ++x, ++y)
		++count;
	if (count >= _winning_length)
		return true;

	count = 1;
	for (int x = row - 1, y = col + 1; x >= 0 && y < static_cast<int>(_width) && _board[x][y] == _current_player; --x, ++y)
		++count;
	for (int x = row + 1, y = col - 1; x < static_cast<int>(_height) && y >= 0 && _board[x][y] == _current_player; ++x, --y)
		++count;
	if (count >= _winning_length)
		return true;

	return false;
}

bool game_board::is_losing_move(unsigned col) const
{
	return _board[0][col] != 0;
}

void game_board::put_stone(unsigned col)
{
	int row;

	if ((row = empty_row(col)) < 0)
		return;

	_board[row][col] = _current_player;
	++_stone_amounts;
	std::swap(_current_player, _last_player);
}

void game_board::take_stone(unsigned col)
{
	if (_stone_amounts == 0)
		return;

	int row;

	if ((row = occupied_row(col)) < 0)
		return;

	if (_board[row][col] != _last_player)
		return;

	_board[row][col] = 0;
	--_stone_amounts;
	std::swap(_current_player, _last_player);
}

void game_board::print_board() const
{
	const std::string clear_str = "\x1B[2J\x1B[H";
	const std::string nocolor = "\e[0m";
	const std::string color[3] = {"\033[0;30m", "\033[0;31m", "\033[0;34m"}; // Black, Red, Blue

	std::cerr << clear_str;
	std::cerr << "height : " << _height << std::endl
			  << "width : " << _width << std::endl
			  << "board size : " << _board_size << std::endl
			  << "winning length : " << _winning_length << std::endl
			  << "stone amounts : " << _stone_amounts << std::endl
			  << "current player : " << _current_player << std::endl
			  << "last player : " << _last_player << std::endl;

	std::cerr << std::endl
			  << std::endl;

	for (int i = 0; i < static_cast<int>(_height); ++i)
	{
		for (int j = 0; j < static_cast<int>(_width); ++j)
		{
			std::cerr << "  " << color[_board[i][j]] << _board[i][j] << nocolor;
		}
		std::cerr << std::endl;
	}

	std::cerr << std::endl;
	for (int i = 0; i < static_cast<int>(_width); ++i)
	{
		if (i < 10)
			std::cerr << "  " << i;
		else
			std::cerr << " " << i;
	}
	std::cerr << std::endl;
}

void game_board::test()
{
	unsigned func, col;
	std::cerr << "0 : is_winning_move" << std::endl
			  << "1 : is_losing_move" << std::endl
			  << "2 : put_stone" << std::endl
			  << "3 : take_stone" << std::endl
			  << "4 : exit" << std::endl;
	do
	{
		std::cerr << "enter the number of your testing functions and the target column : " << std::endl;
		std::cin >> func;
		if (func == 4)
			break;
		std::cin >> col;
	} while (func > 4);
	if (func == 0)
	{
		std::cerr << is_winning_move(col) << std::endl;
		std::cerr << "press enter to continue" << std::endl;
		std::getc(stdin);
		std::getc(stdin);
	}
	else if (func == 1)
	{
		std::cerr << is_losing_move(col) << std::endl;
		std::cerr << "press enter to continue" << std::endl;
		std::getc(stdin);
		std::getc(stdin);
	}
	else if (func == 2)
	{
		put_stone(col);
	}
	else if (func == 3)
	{
		take_stone(col);
	}
	else
	{
		exit(0);
	}
}
