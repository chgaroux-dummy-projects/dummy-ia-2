#include "../includes/utils.hpp"

unsigned const status_max_size = 1e6;

struct VectorHasher
{
	int operator()(const std::vector<std::vector<unsigned> > &V) const
	{
		int hash = V.size();
		for (auto &j : V) for (auto &i : j)
		{
			hash ^= i + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		}
		return hash;
	}
};

std::unordered_map<std::vector<std::vector<unsigned> >, int, VectorHasher> saved_score;
std::queue<std::vector<std::vector<unsigned> > >  saved_status;

// use LRU-cache
// change vector<vector<unsigned> > to vector<bits>

int negamax(game_board &board, int lower_bound, int upper_bound)
{
	if (saved_score.find(board.get_board()) != saved_score.end())
		return saved_score[board.get_board()];
	int width = board.get_width();
	int const max_score = board.get_board_size() - board.get_stone_amounts();

	if (lower_bound < -1 * max_score)
	{
		lower_bound = -1 * max_score;
		if (lower_bound >= upper_bound)
		{
			saved_score[board.get_board()] = lower_bound;
			saved_status.push(board.get_board());
			if (saved_status.size() > status_max_size)
			{
				saved_score.erase(saved_score.find(saved_status.back()));
				saved_status.pop();
			}
			return saved_score[board.get_board()];
		}
	}

	for (int i = 0; i < width; ++i)
		if (board.is_winning_move(i))
		{
			saved_score[board.get_board()] = -1 * max_score;
			saved_status.push(board.get_board());
			if (saved_status.size() > status_max_size)
			{
				saved_score.erase(saved_score.find(saved_status.back()));
				saved_status.pop();
			}
			return saved_score[board.get_board()];
		}

	for (int j = 0; j < width; ++j)
	{
		int i = board.searched_col[j];
		if (board.is_losing_move(i))
		{
			upper_bound = std::min(upper_bound, max_score);
			continue;
		}
		board.put_stone(i);
		int score = -1 * negamax(board, -1 * upper_bound, -1 * lower_bound);
		if (score < lower_bound)
		{
			board.take_stone(i);
			saved_score[board.get_board()] = score;
			saved_status.push(board.get_board());
			if (saved_status.size() > status_max_size)
			{
				saved_score.erase(saved_score.find(saved_status.back()));
				saved_status.pop();
			}
			return saved_score[board.get_board()];
		}
		upper_bound = std::min(upper_bound, score);
		board.take_stone(i);
	}
	saved_score[board.get_board()] = upper_bound;
	saved_status.push(board.get_board());
	if (saved_status.size() > status_max_size)
	{
		saved_score.erase(saved_score.find(saved_status.back()));
		saved_status.pop();
	}
	return saved_score[board.get_board()];
}