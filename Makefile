NAME = player

CLASS = game_board

CLASS_PATH = class/

FILES = main.cpp	utils.cpp

FILES += $(addprefix $(CLASS_PATH), $(addsuffix .cpp, $(CLASS)))

SRCS_PATH = srcs/

SRCS = $(addprefix $(SRCS_PATH), $(FILES))

OBJS = $(SRCS:.cpp=.o)

HEADER_FILES = utils.hpp

HEADER_FILES += $(addprefix $(CLASS_PATH), $(addsuffix .hpp, $(CLASS)))

INCLUDES_PATH = includes/

INCLUDES = $(addprefix $(INCLUDES_PATH), $(HEADER_FILES))

CXX = g++

CXXFLAGS = -O3 -pthread -I $(INCLUDES_PATH) -I $(INCLUDES_PATH)$(CLASS_PATH)

all : $(NAME)

$(NAME) : $(OBJS) $(INCLUDES)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $(NAME)

%.o: %.cpp	$(INCLUDES)
	$(CXX) $(CXXFLAGS) $< -c -o $@

clean : 
	rm -rf $(OBJS)

fclean : 
	rm -rf $(OBJS) $(NAME)

re : fclean all

.PHONY: all clean fclean re

#.SILENT:
