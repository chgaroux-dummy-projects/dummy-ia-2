This is a solver for connect-3
Use command ```make```, and a file called 'player' will be created.
Don't start the game with a very big board, otherwise your computer will explode

Recommended size (width * height) :
3 * 8
3 * 9
3 * 10
3 * 11
3 * 12
3 * 13
3 * 14
4 * 3
4 * 4
4 * 5 (the result remain the same since 4 5 3)
4 * 6
4 * 7
4 * 8
4 * 9
4 * 10
4 * 11